// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.github.googlecodelabs.icon_shop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;

import androidx.core.app.NavUtils;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class ItemCardActivity extends AppCompatActivity {
    public static final String INTENT_EXTRA_ITEM = "item";
    private App.ItemData mItem;
    private TextView mItemDescription;
    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_card);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mItemDescription = (TextView) findViewById(R.id.item_description);

        mItem = App.getItem(getIntent().getStringExtra(INTENT_EXTRA_ITEM));

        FloatingActionButton actionButton = (FloatingActionButton) findViewById(R.id.fab);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItem == null) return;
                App.addItemToCart(mItem);
                Snackbar ok = Snackbar.make(view, "Icon \"" + mItem.mName + "\" added to cart", Snackbar.LENGTH_LONG)
                        .setAction("Home", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                NavUtils.navigateUpFromSameTask(ItemCardActivity.this);
                            }
                        });
                ok.setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        NavUtils.navigateUpFromSameTask(ItemCardActivity.this);
                    }
                });
                ok.show();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (mItem != null) {
            setTitle(mItem.mName + " icon");
            mItemDescription.setText(mItem.mDescription);
        }


    }

    public static void startActivity(Context context, App.ItemData item) {
        Intent intent = new Intent(context, ItemCardActivity.class);
        intent.putExtra(INTENT_EXTRA_ITEM, item.mName);
        context.startActivity(intent);


    }


    @Override
    protected void onResume() {
        super.onResume();
        Bundle params = new Bundle();
        params.putString("screen_name", "Item Details");
        mFirebaseAnalytics.logEvent("open_screen", params);

        if (mItem != null) {
            // Define article with relevant parameters
            Bundle product1 = new Bundle();
            product1.putString(FirebaseAnalytics.Param.ITEM_ID, mItem.mName); //replace with the actual ID. If not available, please use the name instead.
            product1.putString(FirebaseAnalytics.Param.ITEM_NAME, mItem.mName); //the name of the article displayed in the list.
            product1.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "icons"); //the article content category
            product1.putString(FirebaseAnalytics.Param.ITEM_VARIANT, "icons"); //the article content type
            product1.putString(FirebaseAnalytics.Param.ITEM_BRAND, "google");
            product1.putString(FirebaseAnalytics.Param.CURRENCY, "IDR");
            product1.putDouble(FirebaseAnalytics.Param.PRICE, mItem.mPrice); //the number of words on the article.
            product1.putLong(FirebaseAnalytics.Param.QUANTITY, 1); //always 1

            ArrayList items = new ArrayList();
            items.add(product1);
// Don't forget to add the bundle into the items array.

            Bundle ecommerceBundle = new Bundle();
            ecommerceBundle.putBundle("items", product1); // IMPORTANT! Do not change the "items" string.

// Set the relevant transaction-level parameters.
            ecommerceBundle.putString(FirebaseAnalytics.Param.TRANSACTION_ID, "xxx_ABC9894335003"); // the Item ID plus random characters. This is to make each transaction unique.
            ecommerceBundle.putDouble(FirebaseAnalytics.Param.VALUE, mItem.mPrice);
            ecommerceBundle.putString(FirebaseAnalytics.Param.CURRENCY, "IDR");

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE, ecommerceBundle);
//            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, ecommerceBundle);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.item_cart_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                NavUtils.navigateUpFromSameTask(this);
                break;
            }
            case R.id.nav_cart: {
                CartActivity.startActivity(this);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
