// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.github.googlecodelabs.icon_shop;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.segment.analytics.Analytics;
import com.segment.analytics.Properties;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity

    implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView.LayoutManager mItemsLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private Button mSelectProfessionButton;
    private String TAG = "Android_FCM";

    private RecyclerView getItemsRecycleView() {
        return (RecyclerView) findViewById(R.id.items_list);
    }

    private NavigationView getNavigationView() {
        return (NavigationView) findViewById(R.id.nav_view);
    }

    private FloatingActionButton getFloatingActonButton() {
        return (FloatingActionButton) findViewById(R.id.fab);
    }

    private Toolbar getToolbar() {
        return (Toolbar) findViewById(R.id.toolbar);
    }

    private DrawerLayout getDrawerLayout() {
        return (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Log.d("RANDOM", RandomStringUtils.randomAlphanumeric(20).toUpperCase());

        mSelectProfessionButton = (Button) findViewById(R.id.select_profession_button);

        Toolbar toolbar = getToolbar();
        setSupportActionBar(toolbar);

        DrawerLayout drawer = getDrawerLayout();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = getNavigationView();
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView searchResult = getItemsRecycleView();
        searchResult.setHasFixedSize(true);
        mItemsLayoutManager = new LinearLayoutManager(this);
        searchResult.setLayoutManager(mItemsLayoutManager);
        mAdapter = new ItemsListAdapter(App.getDataSet(), false);
        searchResult.setAdapter(mAdapter);

        mSelectProfessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(MainActivity.this, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.menu_profession, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        onProfessionSelected(item);
                        return true;
                    }
                });
                popup.show();
            }
        });
        mSelectProfessionButton.setVisibility(View.GONE);
        AsyncTask<Void, Void, Void> checkProfessionSelected = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                String profession = App.getProfession();
                if (profession == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mSelectProfessionButton.setVisibility(View.VISIBLE);
                        }
                    });
                }
                return null;
            }
        }.execute();

    }

    private void onProfessionSelected(MenuItem item) {
        final String profession = item.getTitle().toString();
        mFirebaseAnalytics.setUserId("ID-12345");

        AsyncTask<Void, Void, Void> save = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                App.setProfession(profession);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSelectProfessionButton.setVisibility(View.GONE);
                    }
                });
                return null;
            }
        }.execute();
    }

    public void showSnackbar(String text) {
        Snackbar.make(getToolbar(), text, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
//        mFirebaseAnalytics.setAnalyticsCollectionEnabled(false);

        Bundle params = new Bundle();
        params.putString(FirebaseAnalytics.Param.SCREEN_NAME, "Home");
        params.putString("page_number", "3");
        params.putString("source", "FIR SDK");

        Properties props = new Properties();
        props.putValue(FirebaseAnalytics.Param.SCREEN_NAME, "Home");
        props.putValue("page_number", "3");
        props.putValue("source", "SE SDK");

//        mFirebaseAnalytics.logEvent("open_screen", params);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_BEGIN, params);
        Analytics.with(this).track("Tutorial Begin", props);

//        mFirebaseAnalytics.logEvent("skip", params);
//        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.TUTORIAL_COMPLETE, params);

        ArrayList items = new ArrayList();

        Bundle product1 = new Bundle();
        product1.putString(FirebaseAnalytics.Param.ITEM_ID, "Sample123"); //replace with the actual ID. If not available, please use the name instead.
        product1.putString(FirebaseAnalytics.Param.ITEM_NAME, "Sample Product 1"); //the name of the article displayed in the list.
        product1.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, "icons"); //the article content category
        product1.putString(FirebaseAnalytics.Param.ITEM_VARIANT, "icons"); //the article content type
        product1.putString(FirebaseAnalytics.Param.ITEM_BRAND, "google");
        product1.putLong(FirebaseAnalytics.Param.INDEX, 1); //position of the item in the list
        items.add(product1);


        Bundle ecommerceBundle = new Bundle();
        ecommerceBundle.putParcelableArrayList("items", items); // IMPORTANT! Do not change the "items" string.
        ecommerceBundle.putString(FirebaseAnalytics.Param.ITEM_LIST    , "feed - terkini"); // list name. Refer to the table above and change the item list name accordingly.

//        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_SEARCH_RESULTS, ecommerceBundle );
//        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM_LIST, ecommerceBundle );

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = getDrawerLayout();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings: {
                showSnackbar("Settings");
                return true;
            }
            case R.id.nav_cart: {
                CartActivity.startActivity(this);
                return true;
            }

            case R.id.share: {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Here is the share content body";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_cart: {
                startActivity(new Intent(this, CartActivity.class));
                break;
            }
            case R.id.nav_manage: {
                showSnackbar("Tools");
                break;
            }
        }

        DrawerLayout drawer = getDrawerLayout();
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
